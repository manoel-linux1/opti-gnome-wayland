# opti-gnome-wayland is no longer being maintained

- opti-gnome-wayland-version: sep 2023

- build-latest: 0.1.2

- Support for the distro: Void-Linux/Ubuntu/Debian/Arch/Artix/Manjaro

- If you are using a distribution based on Ubuntu, Debian, or Arch, the script will work without any issues.

- Use at your own risk

- It can only be executed without superuser and sudo

- It only works on Wayland

- opti-gnome-wayland is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with opti-gnome-wayland, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of opti-gnome-wayland to add additional features.

## Installation

- To install opti-gnome-wayland, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/opti-gnome-wayland.git

# 2. To install the opti-gnome-wayland script, follow these steps

- chmod a+x `installupdate.sh`

- sudo `./installupdate.sh`

# 3. Execute the opti-gnome-wayland script

- `opti-gnome-wayland`

# For uninstall

- chmod a+x `uninstall.sh`

- sudo `./uninstall.sh`

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The opti-gnome-wayland project is currently in development. The latest stable version is 0.1.2. We aim to provide regular updates and add more features in the future.

# License

- opti-gnome-wayland is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of opti-gnome-wayland.
